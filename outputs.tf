# This file should contain all outputs from the module
#
# For every resource the ID should be returned to the caller at minimum

output "health_check_id" { value = "${google_compute_http_health_check.http_health_check.id}" }
output "health_check_self_link" { value = "${google_compute_http_health_check.http_health_check.self_link}" }
output "forward_rule_id" { value = "${google_compute_forwarding_rule.http_forwarding_rule.id}" }
output "forward_rule_self_link" { value = "${google_compute_forwarding_rule.http_forwarding_rule.self_link}" }
output "forwarder_ip" { value = "${google_compute_forwarding_rule.http_forwarding_rule.ip_address}" }
output "fowarder_firewall_id" { value = "${google_compute_firewall.http_forwarder_firewall.id}" }
output "fowarder_firewall_self_link" { value = "${google_compute_firewall.http_forwarder_firewall.self_link}" }
