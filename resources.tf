resource "google_compute_http_health_check" "http_health_check" {
  name                = "${lower(var.product)}-${lower(var.function)}-${lower(var.environment)}-httpcheck"
  request_path        = "/"
  check_interval_sec  = "${var.health_check_interval}"
  healthy_threshold   = "${var.healthy_threshold}"
  unhealthy_threshold = "${var.unhealthy_threshold}"
  timeout_sec         = "${var.health_check_timeout}"
}

resource "google_compute_forwarding_rule" "http_forwarding_rule" {
  name       = "${lower(var.product)}-${lower(var.function)}-${lower(var.environment)}-public-fwdrule"
  target     = "${google_compute_target_pool.default.self_link}"
  port_range = "${var.listen_port}"
}

resource "google_compute_firewall" "http_forwarder_firewall" {
  name    = "${lower(var.product)}-${var.environment}-${var.function}-fwdpublic-fw"
  network = "${var.network}"

  allow {
    protocol = "tcp"
    ports    = ["${var.listen_port}"]
  }

  source_ranges = ["${var.permitted_cidr}"]
  target_tags   = ["${lower(var.product)}-${lower(var.function)}-${lower(var.environment)}","${var.target_tags}"]
}

resource "google_compute_target_pool" "default" {
  name          = "${lower(var.product)}-${lower(var.function)}-${lower(var.environment)}-pool"
  instances     = ["${var.target_pool_instances}"]
  health_checks = ["${google_compute_http_health_check.http_health_check.name}"]
}