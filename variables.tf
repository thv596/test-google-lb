# This file should contain all variables used in the module
# Required variables should be at the top followed by optional variables
#
# All variables should have type defined to prevent inferrence of type

variable "environment" {
  type = "string"
}
variable "product" {
  type = "string"
}
variable "environment_map" {
  type    = "map"
  default = {
    dev   = "Development"
    test  = "Testing"
    stage = "Staging"
    prod  = "Production"
  }
}
variable "function" {
  description = "Functional Tier for ELB"
  type = "string"
}
variable "healthy_threshold" {
  default = "5"
  type    = "string"
}
variable "unhealthy_threshold" {
  default = "5"
  type    = "string"
}
variable "health_check_timeout" {
  default = "10"
  type    = "string"
}
variable "health_check_interval" {
  default = "30"
  type    = "string"
}
variable "listen_port" {
  type = "string"
  default = "80"
}
variable "network"{
  type = "string"
  default = "default"
}
variable "permitted_cidr" {
  type = "list"
  default = ["0.0.0.0/0"]
}
variable "target_tags"{
  type = "list"
  default = []
}
variable "target_pool_instances" {
  type = "list"
  default = []
}